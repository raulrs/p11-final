document.addEventListener('DOMContentLoaded', async function() {
    const videoList = document.getElementById('videoList');
    const videoElement = document.getElementById('videoElement');
    const selectButton = document.createElement('button');
    selectButton.textContent = "Seleccione el video";
    document.body.insertBefore(selectButton, videoList);

    async function loadVideoList() {
        try {
            const videos = ['video.mp4', 'video2.mp4'];
            videoList.innerHTML = '';

            videos.forEach(video => {
                const li = document.createElement('li');
                const button = document.createElement('button');
                button.textContent = video;
                button.addEventListener('click', async function() {
                    console.log(`Clicked on video: ${video}`);
                    await startStream(video);
                });
                li.appendChild(button);
                videoList.appendChild(li);
            });

        } catch (error) {
            console.error('Error al cargar la lista de videos:', error);
        }
    }

    async function startStream(file) {
        try {
            const pc = new RTCPeerConnection();

            // Crear una pista ficticia de video usando un canvas
            const canvas = document.createElement('canvas');
            canvas.width = 640;
            canvas.height = 480;
            const ctx = canvas.getContext('2d');
            ctx.fillStyle = 'black';
            ctx.fillRect(0, 0, canvas.width, canvas.height);

            const videoStream = canvas.captureStream();
            videoStream.getTracks().forEach(track => pc.addTrack(track, videoStream));

            // Crear una pista ficticia de audio usando un AudioContext
            const audioContext = new (window.AudioContext || window.webkitAudioContext)();
            const oscillator = audioContext.createOscillator();
            const audioStream = audioContext.createMediaStreamDestination();
            oscillator.connect(audioStream);
            oscillator.start();

            audioStream.stream.getTracks().forEach(track => pc.addTrack(track, audioStream.stream));

            pc.onicecandidate = (event) => {
                if (event.candidate) {
                    console.log('New ICE candidate: ', event.candidate);
                }
            };

            pc.oniceconnectionstatechange = () => {
                console.log('ICE connection state: ', pc.iceConnectionState);
            };

            pc.ontrack = (event) => {
                if (event.track.kind === 'video') {
                    videoElement.srcObject = event.streams[0];
                    videoElement.play();
                } else if (event.track.kind === 'audio') {
                    const audioElement = document.createElement('audio');
                    audioElement.srcObject = event.streams[0];
                    audioElement.play();
                }
            };

            const offer = await pc.createOffer();
            await pc.setLocalDescription(offer);

            console.log(`Generated SDP offer: ${pc.localDescription.sdp}`);

            const response = await fetch('/offer', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ sdp: pc.localDescription.sdp, file: file })
            });

            const answer = await response.json();
            await pc.setRemoteDescription(new RTCSessionDescription(answer));

        } catch (error) {
            console.error('Error starting stream:', error);
        }
    }

    selectButton.addEventListener('click', loadVideoList);
});
