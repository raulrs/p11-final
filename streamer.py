import traceback
import asyncio
import json
import sys
import cv2
import fractions
import websockets
from aiortc import RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.media import MediaStreamTrack
from aiortc.mediastreams import VideoFrame
import logging
import av

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')

class VideoStreamTrack(MediaStreamTrack):
    kind = "video"

    def __init__(self, file, width=640, height=480):
        super().__init__()
        self.cap = cv2.VideoCapture(file)
        self.frame_time = 1 / self.cap.get(cv2.CAP_PROP_FPS)
        self.start_time = None
        self.width = width
        self.height = height
        if not self.cap.isOpened():
            logging.error(f"No se pudo abrir el archivo de video: {file}")

    async def recv(self):
        if self.start_time is None:
            self.start_time = asyncio.get_event_loop().time()
            self.pts = 0

        # Calcular el tiempo transcurrido y ajustar el frame
        pts, time_base = self.next_timestamp()

        ret, frame = self.cap.read()
        if not ret:
            self.cap.set(cv2.CAP_PROP_POS_FRAMES, 0)
            ret, frame = self.cap.read()

        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        frame = cv2.resize(frame, (self.width, self.height))
        frame = VideoFrame.from_ndarray(frame, format="rgb24")

        # Incrementar pts por el tiempo de un frame
        self.pts += self.frame_time
        frame.pts = pts
        frame.time_base = time_base

        # Calcular el tiempo de dormir para mantener la tasa de cuadros por segundo
        elapsed = asyncio.get_event_loop().time() - self.start_time
        sleep_time = self.pts - elapsed
        if sleep_time > 0:
            await asyncio.sleep(sleep_time)

        return frame

    def next_timestamp(self):
        """
        Genera una nueva marca de tiempo para el frame.
        """
        frame_time = self.frame_time
        pts = int(self.cap.get(cv2.CAP_PROP_POS_FRAMES) * frame_time * 1000)
        time_base = fractions.Fraction(1, 1000)
        return pts, time_base


class AudioStreamTrack(MediaStreamTrack):
    kind = "audio"

    def __init__(self, file):
        super().__init__()
        self.container = av.open(file)
        self.stream = next((s for s in self.container.streams if s.type == 'audio'), None)
        if self.stream is None:
            raise ValueError("No se encontró pista de audio en el archivo")

        self.resampler = av.AudioResampler(
            format='s16', layout='mono', rate=48000
        )
        self.packet_generator = self.container.demux(self.stream)

    async def recv(self):
        while True:
            packet = next(self.packet_generator, None)
            if packet is None:
                self.container.seek(0)
                self.packet_generator = self.container.demux(self.stream)
                continue

            frames = packet.decode()
            for frame in frames:
                frame = self.resampler.resample(frame)
                return frame

streamer_id = 1  # Identificador del streamer para nombrar los archivos SDP

async def handle_offer(data, ws):
    global streamer_id
    try:
        logging.info(f"Procesando oferta para archivo: {data['file']}")

        pc = RTCPeerConnection()
        offer_sdp = data["sdp"]
        offer = RTCSessionDescription(sdp=offer_sdp, type="offer")

        await pc.setRemoteDescription(offer)
        logging.info("Descripción remota configurada correctamente.")

        file_name = data["file"]
        file_path = f"{file_name}"
        logging.info(f"Nombre del archivo recibido: {file_name}")
        logging.info(f"Ruta construida para el archivo: {file_path}")

        video_track = VideoStreamTrack(file_path)
        pc.addTrack(video_track)
        logging.info("Pista de video añadida correctamente.")

        try:
            audio_track = AudioStreamTrack(file_path)
            pc.addTrack(audio_track)
            logging.info("Pista de audio añadida correctamente.")
        except ValueError as e:
            logging.warning(f"No se pudo añadir pista de audio: {e}")

        answer = await pc.createAnswer()
        logging.info(f"Respuesta SDP creada: {answer.sdp}")

        await pc.setLocalDescription(answer)
        logging.info("Descripción local configurada correctamente.")

        response_data = {
            "type": "answer",
            "sdp": pc.localDescription.sdp,
            "file": file_name,
        }

        # Guardar el SDP de la respuesta del streamer
        sdp_answer_file = f"2_streamer-{streamer_id}.sdp"
        with open(sdp_answer_file, "w") as f:
            f.write(response_data["sdp"])
        logging.info(f"Archivo {sdp_answer_file} guardado correctamente.")
        streamer_id += 1

        await ws.send(json.dumps(response_data))
        logging.info("Respuesta enviada al servidor de señalización.")
        logging.info(f"SDP de respuesta enviado: {pc.localDescription.sdp}")
        logging.info("Inicio de comunicación WebRTC.")

    except Exception as e:
        logging.error(f"Error en handle_offer: {e}")
        logging.error(f"Stack trace del error:\n{traceback.format_exc()}")


async def handle_messages(websocket, path):
    async for message in websocket:
        data = json.loads(message)
        logging.info(f"Mensaje recibido en el streamer: {data}")

        if data["type"] == "offer":
            await handle_offer(data, websocket)

async def main(file_name, signal_ip, signal_port, streamer_port):
    logging.info("Comienzo de la ejecución del programa.")
    await register_streamer(signal_ip, signal_port, file_name, streamer_port)

    async with websockets.serve(handle_messages, '0.0.0.0', streamer_port):
        logging.info(f"Streamer escuchando en el puerto {streamer_port}")
        await asyncio.Future()  # Mantén el servidor en ejecución

async def register_streamer(signal_ip, signal_port, file_name, streamer_port):
    uri = f"ws://{signal_ip}:{signal_port}"
    async with websockets.connect(uri) as ws:
        register_data = {
            "type": "register",
            "file": file_name,
            "streamer_port": streamer_port
        }
        await ws.send(json.dumps(register_data))
        logging.info(f"Streamer registrado con el archivo: {file_name} en el puerto: {streamer_port}")

if __name__ == "__main__":
    if len(sys.argv) != 5:
        logging.error("Uso: python streamer.py <file_name> <signal_ip> <signal_port> <streamer_port>")
        sys.exit(1)

    file_name = sys.argv[1]
    signal_ip = sys.argv[2]
    signal_port = int(sys.argv[3])
    streamer_port = int(sys.argv[4])

    asyncio.run(main(file_name, signal_ip, signal_port, streamer_port))
