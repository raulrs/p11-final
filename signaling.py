import asyncio
import json
import websockets
import logging
import sys
from datetime import datetime

# Configuración del logging para incluir milisegundos y salida a stderr
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s %(message)s',
    datefmt='%Y%m%d%H%M%S',
    handlers=[logging.StreamHandler(sys.stderr)]
)

# Estructura en memoria para almacenar información de streamers
directory = {}

async def save_sdp(file_name, sdp):
    try:
        with open(file_name, 'w') as f:
            f.write(sdp)
        logging.info(f"Archivo {file_name} guardado correctamente.")
    except Exception as e:
        logging.error(f"Error al guardar el archivo {file_name}: {e}")

async def handle_messages(websocket, path):
    async for message in websocket:
        data = json.loads(message)
        logging.info(f"Mensaje recibido en el servidor de señalización: {data}")

        try:
            if data["type"] == "register":
                file_name = data["file"]
                streamer_address = websocket.remote_address[0]
                streamer_port = data.get("streamer_port", None)
                if streamer_port:
                    directory[file_name] = (streamer_address, streamer_port)
                    logging.info(f"Registro de streamer: {file_name} en {(streamer_address, streamer_port)}")
                else:
                    logging.error("Falta el puerto del streamer en el registro.")

            elif data["type"] == "offer":
                logging.info(f"SDP recibido (oferta) para archivo {data['file']}: {data['sdp']}")
                file_name = data["file"]
                # Guardar oferta SDP del navegador
                if "2_navegador-1.sdp" not in directory.values():
                    await save_sdp("2_navegador-1.sdp", data['sdp'])
                    directory["2_navegador-1.sdp"] = "guardado"
                else:
                    await save_sdp("2_navegador-2.sdp", data['sdp'])
                    directory["2_navegador-2.sdp"] = "guardado"

                if file_name in directory:
                    streamer_address, streamer_port = directory[file_name]
                    try:
                        logging.info(f"Intentando conectar con el streamer en {(streamer_address, streamer_port)}")
                        async with websockets.connect(f"ws://{streamer_address}:{streamer_port}") as streamer_ws:
                            await streamer_ws.send(json.dumps(data))
                            logging.info(f"SDP enviado al streamer: {(streamer_address, streamer_port)}")
                            response = await streamer_ws.recv()
                            logging.info(f"Respuesta recibida del streamer: {response}")
                            await websocket.send(response)
                            logging.info("Respuesta enviada al cliente.")
                            # Guardar respuesta SDP del streamer
                            if file_name == "video.mp4":
                                await save_sdp("2_streamer-1.sdp", json.loads(response)['sdp'])
                            else:
                                await save_sdp("2_streamer-2.sdp", json.loads(response)['sdp'])
                    except Exception as e:
                        logging.error(f"Error al conectar con el streamer: {e}")
                else:
                    logging.error(f"Streamer no encontrado para el archivo {file_name}")

        except websockets.exceptions.ConnectionClosed as e:
            logging.error(f"Error de conexión cerrada con el cliente: {e}")
            break

        except Exception as e:
            logging.error(f"Error al manejar el mensaje: {e}")

async def main(signal_port):
    async with websockets.serve(handle_messages, '127.0.0.1', signal_port):
        logging.info(f"Servidor de señalización escuchando en ws://127.0.0.1:{signal_port}")
        await asyncio.Future()  # Permanece ejecutando indefinidamente

if __name__ == "__main__":
    import sys

    if len(sys.argv) != 2:
        logging.error("Uso: python signaling.py <signal_port>")
        sys.exit(1)

    signal_port = int(sys.argv[1])
    asyncio.run(main(signal_port))
