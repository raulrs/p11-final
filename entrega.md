# ENTREGA CONVOCATORIA JUNIO
Raúl Ruiz Sevilla

Correo: r.ruizs.2018@alumnos.urjc.es

Vídeo explicativo: https://youtu.be/sF1GZGbyl2s

## Parte básica

El sistema que constituye la parte básica de este proyecto final incluye la implementación de 3 scripts principales: signaling.py, streamer.py y front.py (donde se desarrollan además index.html y client.js) . La función principal que deben cumplir es intercambiar mensajes mediante WebRTC de manera que consigamos en un servidor web reproducir los vídeos.

1. **Signaling.py** --- python signaling.py 12345
- Cuando arrancamos este programa queda esperando mensajes UDP en el puerto indicado, almacena la información de los streamers que se hayan registrado y sirve como servidor de señalización para establecer la conexión WebRTC.
	-Comentario adicional: el funcionamiento de signaling.py es óptimo según el enunciado del proyecto y se ha probado en diferentes escenarios.
	
2. **Front.py** ---  python front.py 8888 localhost 12345
- Al arrancar este programa solicita la lista de vídeos al servidor de señalización y se apoya en la página HTML, que al cargar la página web nos va a aparecer para seleccionar los dos vídeos a elegir. Gestiona principalmente la conexión WebRTC entre los streamers y el servidor de señalización.
	- Comentario adicional: el funcionamiento de front.py es óptimo según el enunciado del proyecto y se ha probado en diferentes escenarios.

3. **Streamer.py** --- python streamer.py video.mp4 localhost 12345 5678
		   python streamer.py video2.mp4 localhost 12345 5679
- Al ejecutar cada streamer en un terminal distinto, este va a registrarse en el servidor de señalización en primer lugar y va a registrar el vídeo. Es capaz de recibir y tratar las ofertas SDP que le llegan desde el servidor frontal a través del servidor de señalización y envía la respuesta SDP que corresponda. Mediante WebRTC envía el fichero de vídeo que se especifique a través de la conexión, dependiendo de si pulsamos en un vídeo o en otro. En mi caso los streamers que lance se registran en el servidor de señalización en el puerto '12345' y escuchan la conexión WebRTC y las ofertas en su propio puerto cada uno, en este caso, 5678 y 5679
	Comentario adicional: el funcionamiento de streamer.py es bueno, pero no llega a ser óptimo según la funcionalidad del ejercicio, ya que no he conseguido establecer correctamente el formato de logs (en los otros dos scripts no he tenido problema). 

4. **Index.html**
Se utiliza como página principal del cliente web, e incluye un elemento de vídeo y botones de selección de vídeo.

5. **Client.js**
Gestiona la lógica de la interfaz de usuario para al hacer click en "Seleccione el vídeo" se desplieguen ambos vídeos para escoger. Cuando seleccionamos el vídeo se genera una oferta en el front y se establece una conexión WebRTC para reproducir el vídeo.

## Comentarios
He corregido algunas funcionalidades que no estaban disponibles con anterioridad:
- Que el vídeo al terminar de reproducirse vuelva a empezar de nuevo. Tabién podemos cambiar la reproducción de un vídeo a otro a nuestro antojo y cuando recargo la página los vídeos se pueden volver a seleccionar y reproducir sin ningún problema.
- El vídeo ha sido editado desde mi teléfono móvil con la aplicación de editor de vídeos por defecto, ya que al trabajar en remoto desde vncweb, el vídeo se veía algo pequeó y lo he comprimido a 4:3.