import asyncio
import logging
import json

import websockets
from aiohttp import web
import sys
import os

# Configuración del logging para incluir milisegundos y salida a stderr
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s %(message)s',
    datefmt='%Y%m%d%H%M%S',
    handlers=[logging.StreamHandler(sys.stderr)]
)

signal_ip = None
signal_port = None
navegador_id = 1  # Identificador del navegador para nombrar los archivos SDP

class FrontProtocol:
    def __init__(self, loop, on_response):
        self.loop = loop
        self.on_response = on_response

    def connection_made(self, transport):
        self.transport = transport
        logging.info("Conexión establecida")

    def datagram_received(self, data, addr):
        message = data.decode()
        logging.info(f"Mensaje recibido de {addr}: {message}")
        self.on_response(message)

    def connection_lost(self, exc):
        logging.info("Conexión perdida")
        if (exc):
            logging.error(f"Error: {exc}")

    def error_received(self, exc):
        logging.error(f"Error recibido: {exc}")

async def request_directory(signal_ip, signal_port, loop):
    try:
        future = loop.create_future()

        def on_response(message):
            if not future.done():
                future.set_result(message)

        transport, protocol = await loop.create_datagram_endpoint(
            lambda: FrontProtocol(loop, on_response),
            remote_addr=(signal_ip, signal_port)
        )

        message = "DIRECTORY_REQUEST".encode()
        transport.sendto(message, (signal_ip, signal_port))
        logging.info("Petición de directorio enviada")

        response = await future
        transport.close()
        logging.info("Recepción de directorio desde el servidor de señalización")
        return json.loads(response)

    except Exception as e:
        logging.error(f"Error en request_directory: {e}")
        return []

async def handle_offer(request):
    global navegador_id
    try:
        data = await request.json()
        sdp = data.get("sdp", "")  # Obtener el SDP del cuerpo de la solicitud
        file = data.get("file", "")

        logging.info(f"Datos recibidos en handle_offer: sdp={sdp}, file={file}")

        if not sdp or not file:
            raise ValueError("Falta el SDP o el nombre del archivo de video en la solicitud")

        # Guardar el SDP de la oferta del navegador
        sdp_offer_file = f"2_navegador-{navegador_id}.sdp"
        with open(sdp_offer_file, "w") as f:
            f.write(sdp)
        logging.info(f"Archivo {sdp_offer_file} guardado correctamente.")

        offer_data = {
            "type": "offer",
            "sdp": sdp,
            "file": file,
            "front_addr": ["127.0.0.1", 8888]  # Dirección del front
        }

        uri = f"ws://{signal_ip}:{signal_port}"
        async with websockets.connect(uri) as ws:
            await ws.send(json.dumps(offer_data))
            logging.info(f"Oferta SDP enviada al servidor de señalización: {offer_data}")

            response = await ws.recv()
            logging.info(f"Respuesta recibida del servidor de señalización: {response}")

            # Guardar el SDP de la respuesta del streamer
            response_data = json.loads(response)
            sdp_answer_file = f"2_streamer-{navegador_id}.sdp"
            with open(sdp_answer_file, "w") as f:
                f.write(response_data.get("sdp", ""))
            logging.info(f"Archivo {sdp_answer_file} guardado correctamente.")

            navegador_id += 1  # Incrementar el identificador del navegador después de procesar la respuesta

            return web.json_response(response_data)

    except Exception as e:
        logging.error(f"Error en handle_offer: {e}")
        logging.error(f"Datos recibidos en la solicitud: {await request.text()}")
        return web.Response(text=str(e), status=500)

async def index(request):
    return web.FileResponse('index.html')

async def get_videos(request):
    try:
        # Directamente devolvemos video.mp4 y video2.mp4
        videos = ['video.mp4', 'video2.mp4']
        logging.info(f"Lista de videos: {videos}")
        return web.json_response(videos)
    except Exception as e:
        logging.error(f"Error al obtener la lista de videos: {e}")
        return web.Response(text="Error al obtener la lista de videos", status=500)

async def main(http_port, _signal_ip, _signal_port):
    global loop, signal_ip, signal_port
    signal_ip = _signal_ip
    signal_port = _signal_port

    loop = asyncio.get_event_loop()

    app = web.Application()
    app.router.add_get('/', index)
    app.router.add_get('/list_videos', get_videos)
    app.router.add_post('/offer', handle_offer)
    app.router.add_static('/', path='.')  # Servir archivos desde la raíz del proyecto

    runner = web.AppRunner(app)
    await runner.setup()
    site = web.TCPSite(runner, '0.0.0.0', http_port)
    logging.info(f"Servidor web escuchando en el puerto {http_port}")
    await site.start()

    try:
        await asyncio.sleep(3600)
    except KeyboardInterrupt:
        pass

if __name__ == '__main__':
    import sys
    if len(sys.argv) != 4:
        logging.error("Uso: python front.py <http_port> <signal_ip> <signal_port>")
        sys.exit(1)

    http_port = int(sys.argv[1])
    signal_ip = sys.argv[2]
    signal_port = int(sys.argv[3])

    asyncio.run(main(http_port, signal_ip, signal_port))
